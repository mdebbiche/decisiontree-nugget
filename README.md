# Decision Tree nugget

Repo containing code and presentation for Decision Tree nugget held on 15/02/2019

Files:

- Decision Tree test.ipynb : the notebook where we will test our decision tree and compare it with sklearn's DecisionTreeClassifier

- decision_tree_init.py : Where you can start coding your decision tree.

- ml_utils.py : a bunch of ML related plot functions (we will use plot_roc_curve in the notebook)

- data/titanic.csv : the data we will use in the notebook to test our decision tree
