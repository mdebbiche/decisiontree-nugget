import numpy as np


class DecisionTree:
    def __init__(self, max_depth=None, depth=1):
        self.max_depth = max_depth
        self.depth = depth
        self.left = None  # left tree
        self.right = None  # right tree
        self.split_feature = None
        self.split_value = None
        self.probability = None

    @property
    def is_leaf_node(self):
        pass

    def fit(self, X, y):
        """Fit the decision tree.

        Args:
            X: Matrix containing the dependent variables.
            y: Target vector.

        Returns:
            None
        """
        pass

    def predict(self, X):
        return self.predict_proba(X)[:, 1] >= 0.5
        pass

    def predict_proba(self, X):
        pass

    def __flow_data_through_tree(self, row):
        """Make an observation follow the appropriate decision tree path.

        Hints :
            - Defined recursively
            - We need to figure out which newt tree to pass the row to (left or right)
            - If we are on the leaf node, we stop and return probability

        Returns : the leaf node probability.

        """
        pass

    def __find_best_split(self, X, y):
        """For every column in X, find the best split.

        Returns :

        best_split = dict() with keys : split_value, split_feature & information_gain
        """
        pass

    def __find_best_split_for_column(self, x, y):
        """Return the best possible split value for a specific column.

        All unique values contained in x are tested as split values.
        If there is only one unique value, just return None, None.


        Returns : (information_gain, split_value)

        """
        pass

    def __calculate_information_gain(self, total_count, left_count, left_impurity,
                                     right_count, right_impurity):
        # Replace with information_gain formula
        information_gain = None

        return information_gain

    def __calculate_impurity_score(self, y):
        """Calculate impurity score (Ginni Index).

        Returns : 0 if y is None or empty.

        """
        if y is None or y.empty:
            return 0

        impurity_score = None
        return impurity_score

    def __create_branches(self, X, y):
        """Create the new branches (left and right trees)"""
        pass
