import numpy as np


class DecisionTree:
    def __init__(self, max_depth=None, depth=1):
        self.max_depth = max_depth
        self.depth = depth
        self.split_feature = None
        self.split_value = None
        self.left = None  # left tree
        self.right = None  # right tree
        self.probability = None

    @property
    def is_leaf_node(self):
        return self.left is None

    def fit(self, X, y):
        """Fit the decision tree.

        Args:
            X: Matrix containing the dependent variables.
            y: Target vector.

        Returns:
            None
        """
        y = y.astype("category")
        if self.max_depth is None:
            self.max_depth = len(X)
        self.impurity_score = self.__calculate_impurity_score(y)

        if self.depth <= self.max_depth and self.impurity_score > 0:
            best_split = self.__find_best_split(X, y)
            self.split_feature = best_split["split_feature"]
            self.split_value = best_split["split_value"]
            self.__create_branches(X, y)

        self.probability = y.value_counts().sort_index().apply(lambda x: x/len(y)).tolist()

    def predict(self, X):
        return self.predict_proba(X)[:, 1] >= 0.5

    def predict_proba(self, X):
        return np.array([self.__flow_data_through_tree(row) for index, row in X.iterrows()])

    def __flow_data_through_tree(self, row):
        """Make an observation follow the appropriate decision tree path.

        Hints :
            - Defined recursively
            - We need to figure out which newt tree to pass the row to (left or right)
            - If we are on the leaf node, we stop and return probability

        Returns : the leaf node probability.

        """
        return None

    def __find_best_split(self, X, y):
        """For every column in X, find the best split.

        Returns :

        best_split = dict() with keys : split_value, split_feature & information_gain
        """
        best_split = {}
        for col in X.columns:
            pass
        return best_split

    def __find_best_split_for_column(self, x, y):
        """Return the best possible split value for a specific column.

        All unique values contained in x are tested as split values.
        If there is only one unique value, just return None, None.


        Returns : (information_gain, split_value)

        """
        unique_values = x.unique()
        if len(unique_values) == 1:
            return None, None
        best_information_gain = None
        information_gain = None
        split_value = None

        return information_gain, split_value

    def __calculate_information_gain(self, total_count, left_count, left_impurity,
                                     right_count, right_impurity):
        # Replace with information_gain formula
        information_gain = None

        return information_gain

    def __calculate_impurity_score(self, y):
        """Calculate impurity score (Ginni Index).

        Returns : 0 if y is None or empty.

        """
        if y is None or y.empty:
            return 0

        impurity_score = None
        return impurity_score

    def __create_branches(self, X, y):
        """Create the new branches (left and right trees)"""
        self.left = DecisionTree(max_depth=self.max_depth, depth=self.depth + 1)
        self.right = DecisionTree(max_depth=self.max_depth, depth=self.depth + 1)
        left_X = X[X[self.split_feature] <= self.split_value]
        left_y = y[X[self.split_feature] <= self.split_value]
        right_X = X[X[self.split_feature] > self.split_value]
        right_y = y[X[self.split_feature] > self.split_value]
        self.left.fit(left_X, left_y)
        self.right.fit(right_X, right_y)
